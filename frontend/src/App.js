import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Choose your backend :
          <ul>
            <li>Python (flask)</li>
            <li>Node.js</li>
            <li>Java</li>
            <li>C#</li>
            <li>Rust</li>
          </ul>
        </p>
      </header>
    </div>
  );
}

export default App;
